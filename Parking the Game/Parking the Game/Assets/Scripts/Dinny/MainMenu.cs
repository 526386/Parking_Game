﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    //start game
	public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    //Load How to play scene
    public void HowtoPlay()
    {
        SceneManager.LoadScene("HowToPlay");
    }
    //back button in How to Play
    public void HowToPlayQuit()
    {
        SceneManager.LoadScene("MainMenu");

    }
    // quit game
    public void QuitGame()
    {
        Application.Quit();
    }
}

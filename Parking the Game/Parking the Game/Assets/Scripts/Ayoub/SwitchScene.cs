﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour {

    [SerializeField] string m_LoadScene;

	public void LoadScene()
    {
        SceneManager.LoadScene(m_LoadScene);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acceleration : MonoBehaviour {

    Rigidbody2D rb;

    [SerializeField]
    float AccelerationPower = 5f;
    [SerializeField]
    float Steeringpower = 5f;
    float Steeringamount, Speed, Direction;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Steeringamount = Input.GetAxis("Horizontal");
        Speed = Input.GetAxis("Vertical") * AccelerationPower;
        Direction = Mathf.Sign(Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.up)));
        rb.rotation += Steeringamount * Steeringpower * rb.velocity.magnitude * Direction;

        //Car movement
        rb.AddRelativeForce(Vector2.up * Speed);

        //Add force when steering
        rb.AddRelativeForce(-Vector2.right * rb.velocity.magnitude * Steeringamount / 2);
	}
}

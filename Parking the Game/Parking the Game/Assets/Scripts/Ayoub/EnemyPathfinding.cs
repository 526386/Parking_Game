﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathfinding : MonoBehaviour {

    public Transform[] m_Waypoint;
    public float m_Speed;

    public int m_CurrentLocation;

    Vector2 pos;

    // Update is called once per frame
    void Update () {
        float m_AngleRadius = Mathf.Atan2(m_Waypoint[m_CurrentLocation].transform.position.y - transform.position.y, m_Waypoint[m_CurrentLocation].transform.position.x - transform.position.x);

        float m_AngleDegree = (180 / Mathf.PI) * m_AngleRadius;

        this.transform.rotation = Quaternion.Euler(0, 0, m_AngleDegree - 90);

        if (transform.position != m_Waypoint[m_CurrentLocation].position)
        {
            pos = Vector2.MoveTowards(transform.position, m_Waypoint[m_CurrentLocation].position, m_Speed * Time.deltaTime);
            GetComponent<Rigidbody2D>().MovePosition(pos);
        }
        else if(transform.position == m_Waypoint[m_CurrentLocation].position)
        {
            m_CurrentLocation = (m_CurrentLocation + 1) % m_Waypoint.Length;
        }
        else
        {
            m_CurrentLocation = (m_CurrentLocation + 1) % m_Waypoint.Length;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acc_Sound : MonoBehaviour {

    public AudioClip Acceleration;

    AudioSource Audio;

	// Use this for initialization
	void Start () {
        Audio = GetComponent<AudioSource>();
        Audio.Stop();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W))
        {
            Audio.enabled = true;
            Audio.loop = true;
        }
        else
        {
            Audio.enabled = false;
            Audio.loop = false;
        }
    }
}

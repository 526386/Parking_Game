﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OffScreenTimer : MonoBehaviour
{

    // Dit is een copy van Dinny's timer script. De update is veranderd naar OnTriggerStay2D

    public float m_StartTime;
    private float m_CurrentTime;
    public Text m_CountDownText;
    bool m_TimerActive;

    // Use this for initialization
    void Start()
    {
        m_CurrentTime = m_StartTime;

        m_CountDownText.gameObject.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            m_TimerActive = true;
            m_CountDownText.gameObject.SetActive(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            m_CurrentTime = m_StartTime;
            m_TimerActive = false;

            m_CountDownText.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (m_TimerActive == true && m_CurrentTime >= -1)
        {
            string seconds = (m_CurrentTime % 60).ToString("f2");

            m_CountDownText.text = ("Time Left = " + seconds);

            if (m_CurrentTime <= 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            m_CurrentTime -= 1 * Time.deltaTime;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveScene : MonoBehaviour
{

    [SerializeField]
    private string LoadLevel;

    [SerializeField]
    private string Wheel;
    private void Update()
    {
        int Number = transform.childCount;
        for (int i = 0; i < Number; i++)
        {
            if (transform.GetChild(i).name == Wheel)
            {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Wheel))
        {
            SceneManager.LoadScene(LoadLevel);
        }
    }
}

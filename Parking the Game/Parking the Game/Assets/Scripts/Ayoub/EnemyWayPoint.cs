﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EnemyWayPoint : MonoBehaviour {

    public List<Transform> Waypoint = new List<Transform>();

    int Index;

    bool RunInGame;
	
	void Update () {
        // Add childs to waypoint list

        Transform[] tem = GetComponentsInChildren<Transform>();

        if(!Application.isPlaying)//Ability to stop updating waypoints during play mode
        {
            if (tem.Length > 0)
            {
                Waypoint.Clear();
                Index = 0;

                foreach (Transform t in tem)
                {
                    if (t != transform)
                    {
                        Index++;
                        t.name = "Point " + Index.ToString();

                        Waypoint.Add(t);
                    }
                }
            }
        }
	}

    private void OnDrawGizmos()
    {
        if(Waypoint.Count > 0)
        {
            Gizmos.color = Color.green;

            foreach(Transform t in Waypoint)
            {
                Gizmos.DrawSphere(t.position, 1f);
            }

            Gizmos.color = Color.red;
            
            // Loop for waypoint list
            for(int i = 0; i < Waypoint.Count - 1; i++)
            {
                Gizmos.DrawLine(Waypoint[i].position, Waypoint[i + 1].position);
            }
        }
    }
}
